-- SUMMARY --
-------------
Simply menu module is based on menu core module. It automatically associates a content to item menu, without set link path any more. So, the menu administration interface becomes a way to create the site skeleton.


-- FEATURES --
--------------
- When site editors create a new menu item, a new content is created. 

- Site editors can create containers menu which redirect visitor to the first child page. 

- Moreover, on node create form, default menu settings is to create a new menu item, which is disabled by default. So, all site map can be visible in one interface. Editors can choose to 'show/hide disabled items' (hide by default).


-- INSTALLATION --
------------------
1) Copy simply_menu directory to your modules directory
2) Enable the module at: /admin/build/modules


-- CONTACT --
-------------
Current maintainer:
  * C.Patrix (Makina Corpus, France) - http://drupal.org/user/786984
  
This project has been sponsored by:
  * Makina Corpus
    Friendly Drupal experts providing professional consulting & training.
    Visit http://www.makina-corpus.com for more information.
