<?php
// $Id$

/**
 * @file
 * This module provides a simple way to use menu to create your site skeleton and position your nodes.
 * The initial development of this module was sponsered by Makina Corpus, France
 *
 *
 * @author cpat
 *
 *TODO: Add external link menu
 */


/**
 * Implementation of hook_help().
 */
function simply_menu_help($path, $arg = NULL) {
  $output = "";
  switch ($path) {
    case 'admin/help#simply_menu':
      $output = '<p>' . t("Simply menu module is based on menu core module. It automatically associates a content to item menu, without set link path any more. So, the menu administration interface becomes a way to create the site skeleton.") . '</p>';
      $output .= '<p>' . t("When site editors create a new menu item, a new content is created. Site editors can create containers menu which redirect visitor to the first child page. Moreover, on node create form, default menu settings is to create a new menu item, which is disabled by default. So, all site map can be visible in one interface. Editors can choose to 'show/hide disabled items' (hide by default).") . '</p>';
      break;

    case 'admin/structure/menu/manage/%':
      $output = "<p>" . t("Here, you can <b>create, organize and manage your content</b>. To create new content, use the <b>'Add content'</b> link. You can organize your <b>menu hierarchy by drag and drop</b> (don't forget to save your modifications). You can create content, <b>container menu item or a new front page</b> (See details on 'Add content' link page).") . "</p>";
      break;
  }

  return $output;
}

/**
 * Implementation of hook_menu().
 */
function simply_menu_menu() {
  $items['simply_menu_container/%/%'] = array(
    'title' => 'Redirect page',
    'description' => 'Redirect page to child for Containers.',
    'access callback' => 'simply_menu_container_access',
    'access arguments' => array(1, 2),
    'page callback' => 'simply_menu_container',
    'page arguments' => array(1, 2),
    'type' => MENU_CALLBACK,
  );

  $items['simply_menu_node_status/%/%/%'] = array(
    'title' => 'Publish/Unpublish node',
    'description' => 'Publish/Unpublish node associated to menu item.',
    'access callback' => 'simply_menu_publish_access',
    'access arguments' => array(2),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('simply_menu_node_status_confirm', 1, 2, 3),
    'type' => MENU_CALLBACK,
  );

  $items['admin/structure/menu/manage/%/disabled/show'] = array(
    'title' => 'Show disabled items',
    'description' => 'Show enabled and disabled menu items.',
    'page callback' => 'simply_menu_show_disabled',
    'page arguments' => array(4),
    'access callback' => 'simply_menu_show_disabled_access',
    'type' => MENU_LOCAL_ACTION,
  );

  $items['admin/structure/menu/manage/%/disabled/hide'] = array(
    'title' => 'Hide disabled items',
    'description' => 'Show enabled and disabled menu items.',
    'page callback' => 'simply_menu_hide_disabled',
    'page arguments' => array(4),
    'access callback' => 'simply_menu_hide_disabled_access',
    'access arguments' => array(6),
    'type' => MENU_LOCAL_ACTION,
  );

  return $items;
}

/*********** Access callback functions *****************/

/**
 * Implements hook_menu callback access function.
 * Access control to display a container menu. 
 * A container menu isn't displayed if there is no sub-menu item or
 * if the current user haven't access to sub-menu item.
 */
function simply_menu_container_access($menu, $item) {
  //if admin or administer menu permission display container
  global $user;
  if ($user->uid == 1 || user_access('administer nodes')) {
    return TRUE;
  }
  else {
    ////if no child, don't display
    $query = db_select("menu_links", "ml")
        ->fields("ml", array("has_children"))
        ->condition("mlid", $item)
        ->condition("menu_name", $menu)
        ->execute();
    $child = $query->fetchField();
    if (!$child) {
      return FALSE;
    }

    ////if none published child, don't display
    $links = simply_menu_get_child($menu, $item);
    if (count($links) == 0) {
      return FALSE;
    }
    else {
      return TRUE;
    }
  }
}

/**
 * Implements hook_menu callback access function.
 * Define if the current user can publish the content associated to the menu item.
 * If he can, publish/unpublish button is displayed.
 */
function simply_menu_publish_access($node) {
  return node_access('update', $node);
}

/**
 * Implements hook_menu callback access function.
 * Define if the user asks to see disabled menu item
 */
function simply_menu_show_disabled_access() {
  if (!isset($_GET["display"])) {
    return TRUE;
  }
  else {
    return FALSE;
  }
}

/**
 * Implements hook_menu callback access function.
 * Define if the user asks to hide disabled menu item
 */
function simply_menu_hide_disabled_access($arg) {
  if (isset($_GET["display"]) || $arg == "hide") {
    return TRUE;
  }
  else {
    return FALSE;
  }
}

/*********** Form alter, validate, submit functions *****************/

/**
 * Implementation of hook_form_alter().
 */
function simply_menu_form_alter(&$form, &$form_state, $form_id) {
    global $base_url;

    if ($form_id == "menu_edit_item") {
      //if come from add child button, set parent field
      if (isset($_GET["plid"])) {
        $plid = check_plain($_GET["plid"]);
        $menu = $form["original_item"]["#value"]["menu_name"];
        $opt = $menu . ":" . $plid;
        $form["parent"]["#default_value"] = $opt;
        unset($form["parent"]);
        $form["parent"] = array(
          "#type" => "value",
          "#value" => $opt,
        );
      }

      //alter add/edit menu item form + action on validation
      if (isset($form["link_path"]["#default_value"])) {
        $link_path = $form["link_path"]["#default_value"];
      }
      else {
        $link_path = "";
      }
      unset($form["link_path"]);
      $form["link_path"] = array(
        "#type" => "value",
        "#value" => $link_path,
      );
      $form["link_path"]["#required"] = 0;
      $form["link_path"]["#disabled"] = 1;
      $desc = $form["description"]["#default_value"];
      unset($form["description"]);
      $form["description"] = array(
        "#type" => "value",
        "#value" => $desc,
      );
      $exp = $form["expanded"]["#default_value"];
      unset($form["expanded"]);
      $form["expanded"] = array(
        "#type" => "value",
        "#value" => $exp,
      );
      $weight = $form["weight"]["#default_value"];
      unset($form["weight"]);
      $form["weight"] = array(
        "#type" => "value",
        "#value" => $weight,
      );

      if ($form["mlid"]["#value"] == 0) {
        $form["page_title"] = array(
            "#type" => "textfield",
            "#title" => t("Page title"),
            "#default_value" => "",
            "#description" => t("Page title which is associated to this menu item. If you just want create a container which display the first child on click, set page title to '&lt;container&gt;'."),
            "#required" => TRUE,
            "#weight" => 0,
        );

        $opt = node_type_get_names();
        $tab["container"] = t("Container");
        $tab["front"] = t("Front page");
        $opt = array_merge($tab, $opt);
        $form["page_type"] = array(
            "#type" => "select",
            "#title" => t("Page type"),
            "#description" => t("Page type which is associated to this menu item. If you just want create a container which display the first child on click, choose 'Container' to page type field. If you want create a front page menu item, choose 'Front page'."),
            '#options' => $opt,
            "#required" => TRUE,
            "#weight" => 0,
        );
        $form["#prefix"] = "<div class='messages warning'>" . t("<p><b>NOTE : </b>If you just want create a container which display the first child on click, set page title to '&lt;container&gt;' and choose 'Container' to page type field. If you want create a front page menu item, choose 'Front page'.</p>") . "</div>";
      }

      $form["#validate"][] = "simply_menu_alter_item_validate_form";
      $form["#validate"] = array_reverse($form["#validate"]);
      $form["#submit"][] = "simply_menu_alter_item_submit_form";
    }

    if ($form_id == "menu_overview_form") {
      $menu = $form['#menu']['menu_name'];
      $path_menu = 'admin/structure/menu/manage/'. $menu;

      //Add unpubished items
      $form_init = $form;
      $form = array();

      $sql = "
        SELECT m.load_functions, m.to_arg_functions, m.access_callback, m.access_arguments, m.page_callback, m.page_arguments, m.delivery_callback, m.title, m.title_callback, m.title_arguments, m.type, m.description, ml.*
        FROM {menu_links} ml LEFT JOIN {menu_router} m ON m.path = ml.router_path
        WHERE ml.menu_name = :menu
        ORDER BY p1 ASC, p2 ASC, p3 ASC, p4 ASC, p5 ASC, p6 ASC, p7 ASC, p8 ASC, p9 ASC";
      $result = db_query($sql, array(':menu' => $menu), array('fetch' => PDO::FETCH_ASSOC));
      $links = array();
      foreach ($result as $item) {
        $links[] = $item;
      }
      $tree = menu_tree_data($links);
      $node_links = array();
      menu_tree_collect_node_links($tree, $node_links);
      // We indicate that a menu administrator is running the menu access check.
      global $menu_admin;
      $menu_admin = TRUE;
      $tree = simplymenu_tree_check_noaccess($tree, $node_links);
      $menu_admin = FALSE;

      $form = array_merge($form, _menu_overview_tree_form($tree));
      unset($form["#tree"]);
      uasort($form, 'simply_menu_compare_weight');
      $form = array_reverse($form);

      //child location
      $child_tab = array();
      foreach ($form as $k => $item) {
        $plid = $item["plid"]["#default_value"];
        if ( $plid != 0) {
          $child_tab[$plid][$k] = $item;
          unset($form[$k]);
        }
      }

      //place childs
      while (count($child_tab) > 0) {
        foreach ($child_tab as $plid => $childs) {
          $keys_tab = array_keys($form);
          foreach ($keys_tab as $idx => $key) {
            if ($key === "mlid:" . $plid) {
              $offset = $idx + 1;
              array_splice($form, $offset, 0, $childs);
              unset($child_tab[$plid]);
            }
          }
        }

        //rename keys
        $form_new = array();
        foreach ($form as $k => $item) {
          if (!preg_match("!mlid:!", $k)) {
            if (isset($item["#item"]["mlid"])) {
              $form_new["mlid:" . $item["#item"]["mlid"]] = $item;
            }
            else {
              $form_new[$k] = $item;
            }
          }
          else {
            $form_new[$k] = $item;
          }
        }
        $form = $form_new;
      }

      //add complementary informations
      foreach ($form_init as $k => $item) {
        if (preg_match("!mlid:!", $k)) {
          unset($form_init[$k]);
        }
      }
      $form = array_merge($form_init, $form);

      
      //Hide / Show disabled menus
      if (!isset($_GET["display"])) {
        foreach ($form as $k => $item) {
          if (preg_match("!mlid:!", $k)) {
              if ($item["hidden"]["#default_value"] != 1) {
                unset($form[$k]);
              }
          }
        }
      }

      //add "add child" and published/unpublished actions and modify others operations
      foreach ($form as $k => $item) {
        if (preg_match("!mlid:!", $k)) {
          if (isset($form[$k]["#item"]["module"]) && $form[$k]["#item"]["module"] == "system") {
            //$form[$k]["operations"]["delete"] = array("#type" => "link", "#title" => "", "#href" => "");
            if (isset($form[$k]["operations"]["edit"])) {
              $form[$k]["operations"]["edit"] = array("#type" => "link", "#title" => "", "#href" => "");
            }
            if (isset($form[$k]["operations"]["reset"])) {
              unset($form[$k]["operations"]["reset"]);
            }
          }
          if ($item["#item"]["router_path"] == "node/%") {
            //modify delete operation
            $form[$k]["operations"]["delete"]["#href"] = $item["#item"]["link_path"] . "/delete";
            $form[$k]["operations"]["delete"]["#options"] = array('query' => array("destination" => $path_menu));

            //get node status
            $path = $item["#item"]["link_path"];
            $nid = preg_replace("!node/(\d+)$!", "$1", $path);
            if ($nid != $path) {
              $node = node_load($nid);
              $status = $node->status;
            }

            //modify edit operation
            $form[$k]["operations"]["edit"]["#href"] = $item["#item"]["link_path"] . "/edit";
            $form[$k]["operations"]["edit"]["#options"] = array('query' => array("destination" => $path_menu));

            if (isset($status)) {
              if ($status) {
                //unpublish
                $url = "simply_menu_node_status/unpublish/" . $nid . "/" . $menu;
                $link = l(t("unpublish"), $url, array("attributes" => array("class" => "btn_unpublish")));
                $form[$k]["operations"]["edit"]["#suffix"] = "<br/>" . $link;
              }
              else {
                //publish
                $url = "simply_menu_node_status/publish/" . $nid . "/" . $menu;
                $link = l(t("publish"), $url, array("attributes" => array("class" => "btn_publish")));
                $form[$k]["operations"]["edit"]["#suffix"] = "<br/>" . $link;
              }
            }
          }

          //Add child button
          $url = $path_menu . "/add";
          $link = l(t("add child"), $url, array("query" => array("plid" => $form[$k]["mlid"]["#value"]), "attributes" => array("class" => "btn_child")));
          $suffix = "";
          if (isset($form[$k]["operations"]["edit"]) && isset($form[$k]["operations"]["edit"]["#suffix"])) {
            $suffix = $form[$k]["operations"]["edit"]["#suffix"];
          }
          $form[$k]["operations"]["edit"]["#suffix"] = $suffix . "<br/>" . $link;
        }
      }
    }

    if ($form_id == "node_delete_confirm") {
      $path = "node/" . $form["nid"]["#value"];
      $front = variable_get("site_frontpage");
      if ($path == $front) {
        $form["#prefix"] = "<div class='messages warning'>" . t("Becarfull : This content is your front page. If you delete it, you will have to create a new front page content.") . "</div>";
        $form["#submit"][] = "simply_menu_delete_front";
      }
    }

    //alter menu settings in node form
    foreach (node_type_get_names() as $type => $name) {
      if ($form_id == $type . "_node_form") {
        if (is_null($form["nid"]["#value"])) {
          $form["menu"]["enabled"]["#default_value"] = 1;
        }
        unset($form["menu"]["link"]["description"]);
        $form["menu"]["link"]["description"] = array(
                "#type" => "value",
                "#value" => "",
              );
         $weight = $form["menu"]["link"]["weight"]["#default_value"];
         unset($form["menu"]["link"]["weight"]);
         $form["menu"]["link"]["weight"] = array(
                "#type" => "value",
                "#value" => $weight,
              );

         //Add disabled checkbox
         if (is_null($form["nid"]["#value"])) {
           $hidden = 1;
         }
         else {
           $hidden = $form["menu"]["link"]["hidden"]["#value"];
         }
         $form["menu"]["link"]["hidden"] = array(
            "#type" => "checkbox",
            "#title" => t("Disabled"),
            "#default_value" => $hidden,
            "#description" => t("Menu links that are not enabled will not be listed in any menu."),
              );

          //Add front page checkbox
          $front = 0;
          if (!is_null($form["nid"]["#value"])) {
            $node_path = "node/" . $form["nid"]["#value"];
            $front_path = variable_get("site_frontpage");
            if ($front_path == $node_path) {
              $front = 1;
            }
          }
          $form["options"]["front"] = array(
            "#type" => "checkbox",
            "#title" => t("Use as front page"),
            "#default_value" => $front,
              );
           $form["#submit"][] = "simply_menu_add_front";
      }
    }
}

/*
 * function callback on alter item menu form validation
 */
function simply_menu_alter_item_validate_form(&$form, &$form_state) {

  if (isset($form_state['values']["page_type"]) && isset($form_state['values']["page_title"])) {
    $page_title = $form_state['values']["page_title"];
    $page_type = $form_state['values']["page_type"];

    if ($page_title != "<container>" && $page_type != "container") {

      //Create node
      $title = $form_state['values']["page_title"];
      if ($page_type == "front") {
        $type = "page";
      }
      else {
        $type = $form_state['values']["page_type"];
      }
      $node = new stdClass();
      $node->type = $type;
      node_object_prepare($node);
      $node->title = $title;
      $node->language = LANGUAGE_NONE;
      if (module_exists('print')) {
        //print module compatibility
        $node->print_display = NULL;
        $node->print_display_comment = NULL;
        $node->print_display_urllist = NULL;
      }
      node_save($node);

      if (module_exists('pathauto')) {
        //pathauto module compatibility
        module_load_include('inc', 'pathauto');
        $uri = entity_uri('node', $node);
        $pathauto_alias = pathauto_create_alias('node', 'return', $uri['path'], array('node' => $node), $node->type, $node->language);
        $node->path = array('pathauto' => TRUE, 'alias' => $pathauto_alias);
        node_save($node);
      }

      //Set link path
      $form["link_path"]["#value"] = "node/" . $node->nid;
      $form_state['values']["link_path"] = "node/" . $node->nid;

      //set site_frontpage variable
      if ($page_type == "front") {
        variable_set('site_frontpage', 'node/' . $node->nid);
      }

    }
    else {
      //container menu item
      $menu_name = $form["original_item"]["#value"]["menu_name"];
      $form["link_path"]["#value"] = "simply_menu_container/" . $menu_name . "/";
      $form_state['values']["link_path"] = "simply_menu_container/" . $menu_name . "/";
    }
  }
}

/*
 * function callback on alter item menu form submit
 */
function simply_menu_alter_item_submit_form(&$form, &$form_state) {
  $mlid = $form_state['values']["mlid"];
  $menu_name = $form["original_item"]["#value"]["menu_name"];
  $path = $form_state['values']["link_path"];

  if (preg_match("!simply_menu_container!", $path)) {
    $url = $path . $mlid;
    db_update("menu_links")
          ->fields(array('link_path' => $url))
          ->condition("menu_name", $menu_name)
          ->condition("mlid", $mlid)
          ->execute();
  }
}

/**
 * Implementation of hook_menu_local_tasks_alter().
 */
function simply_menu_menu_local_tasks_alter(&$data, $router_item, $root_path) {
  foreach ($data["actions"]["output"] as $k => $act) {
    if ($act["#link"]["path"] == "admin/structure/menu/manage/%/add") {
      $data["actions"]["output"][$k]["#link"]["title"] = t("Add content");
    }
  }
}

/*********** Other callback functions *****************/

/**
 * Container page callback
 */
function simply_menu_container($menu, $plid) {
  $links = simply_menu_get_child($menu, $plid);
  $mlids = array();
  foreach ($links as $tab) {
    $mlids[] = $tab["mlid"];
  }

  $query = db_select("menu_links", "ml")
        ->fields("ml", array("weight"))
        ->condition("menu_name", $menu)
        ->condition("mlid", $mlids, "IN")
        ->orderby("weight", "ASC")
        ->range(0, 1)
        ->execute();
  $weight = $query->fetchField();

  $child = db_select("menu_links", "ml")
              ->fields("ml", array("link_path"))
              ->condition("menu_name", $menu)
              ->condition("mlid", $mlids, "IN")
              ->condition("weight", $weight)
              ->execute();
  $child_path =  $child->fetchField();
  drupal_goto($child_path);
}

/**
 * Implementation callback function().
 * Check access to build menu tree
 */
function simplymenu_tree_check_noaccess($tree, $node_links) {
  if ($node_links) {
    $nids = array_keys($node_links);
    $select = db_select('node', 'n');
    $select->addField('n', 'nid');
    //$select->condition('n.status', 0);
    $select->condition('n.nid', $nids, 'IN');
    $select->addTag('node_access');
    $nids = $select->execute()->fetchCol();
    foreach ($nids as $nid) {
      foreach ($node_links[$nid] as $mlid => $link) {
        $node_links[$nid][$mlid]['access'] = TRUE;
      }
    }
  }

  _menu_tree_check_access($tree);

  return $tree;

}

/**
 * Implementation callback function().
 * Compare weight to build menu tree in good order
 */
function simply_menu_compare_weight($a, $b) {
  return strnatcmp($a['weight']["#default_value"], $b['weight']["#default_value"]);
}

/**
 * Implementation callback hook_menu function().
 * confirm form to change the content status
 */

function simply_menu_node_status_confirm($form, $form_state, $status, $nid, $menu) {

  global $base_url;

  $form['status'] = array(
		'#type' => 'value',
		'#value' => check_plain($status),
	);

	$form['nid'] = array(
		'#type' => 'value',
		'#value' => check_plain($nid),
	);

	$form['menu'] = array(
		'#type' => 'value',
		'#value' => check_plain($menu),
	);

	$question = "";
	switch ($status) {
	  case "publish":
	    $question = t('Are you sure you want to publish this content ?');
	    break;

	  case "unpublish":
	    $question = t('Are you sure you want to unpublish this content ?');
	    break;
	}

	return confirm_form($form,
    	$question,
    	$base_url . "/admin/structure/menu/manage/" . $menu
      );
}


/**
 * Implementation callback hook_menu function().
 * submit confirm form to change the content status
 */
function simply_menu_node_status_confirm_submit(&$form, &$form_state) {
  $status = $form_state['values']["status"];
  $nid = $form_state['values']["nid"];
  $menu = $form_state['values']["menu"];
  $node = node_load($nid);

  switch ($status) {
    case "unpublish":
      $node->status = 0;
      node_save($node);
      drupal_set_message(t('Your content has been unpublished.'));
    break;

    case "publish":
      $node->status = 1;
      node_save($node);
      drupal_set_message(t('Your content has been published.'));
    break;
  }
  drupal_goto("/admin/structure/menu/manage/" . $menu);
}

/**
 * Implementation callback hook_menu function().
 * undefined content front page
 */
function simply_menu_delete_front(&$form, &$form_state) {
  variable_set("site_frontpage", "node");
}

/*
 * function callback on alter node form submit
 * Add content as front page
 */
function simply_menu_add_front(&$form, &$form_state) {
  $front = $form_state['values']["front"];
  $node_path = "node/" . $form["nid"]["#value"];
  $front_path = variable_get("site_frontpage");
  if ($front) {
    if ($front_path != $node_path) {
      $nid = $form["nid"]["#value"];
      variable_set("site_frontpage", "node/" . $nid);
    }
  }
  else {
    if ($front_path == $node_path) {
      variable_set("site_frontpage", "node");
    }
  }
}

/**
 * Implements hook_menu callback function.
 * display all menu items enabled and disabled
 */
function simply_menu_show_disabled($menu) {
  global $base_url;

  $url = url($base_url . "/admin/structure/menu/manage/" . $menu, array("query" => array("display" => "all")));
  drupal_goto($url);
}

/**
 * Implements hook_menu callback function.
 * Hide all disabled menu item
 */
function simply_menu_hide_disabled($menu) {
  global $base_url;

  $url = url($base_url . "/admin/structure/menu/manage/" . $menu);
  drupal_goto($url);
}

/**
 * Implements callback function.
 * Build menu item tree
 */
function simply_menu_get_item_tree($menu, $item) {
  $query = db_select('menu_links', 'ml', array('fetch' => PDO::FETCH_ASSOC));
  $query->addTag('translatable');
  $query->leftJoin('menu_router', 'm', 'm.path = ml.router_path');
  $query->fields('ml');
  $query->fields('m', array(
    'load_functions',
    'to_arg_functions',
    'access_callback',
    'access_arguments',
    'page_callback',
    'page_arguments',
    'delivery_callback',
    'tab_parent',
    'tab_root',
    'title',
    'title_callback',
    'title_arguments',
    'theme_callback',
    'theme_arguments',
    'type',
    'description',
  ));
  for ($i = 1; $i <= 4; $i++) {
    $query->orderBy('p' . $i, 'ASC');
  }
  $query->condition('ml.menu_name', $menu);
  $query->condition('ml.plid', array($item), 'IN');
  $links = array();
  foreach ($query->execute() as $row) {
    $links[] = $row;
  }

  return $links;
}

/**
 * Implements callback function.
 * Get menu item tree child
 */
function simply_menu_get_child($menu, $item) {
  $container = TRUE;
  $parent = $item;
  $links = simply_menu_get_item_tree($menu, $parent);
  while ($container) {
    $container = FALSE;
    foreach ($links as $k => $val) {
      if  ($val["router_path"] == "node/%" && !user_access('administer nodes')) {
        $nid = str_replace("node/", "", $val["link_path"]);
        $node = node_load($nid);
        if ($node->status == 0) {
          unset($links[$k]);
        }
      }
      if ($val["router_path"] == "simply_menu_container/%/%") {
        $parent = str_replace("simply_menu_container/" . $menu . "/" , "", $val["link_path"]);
        $links = array_merge($links, simply_menu_get_item_tree($menu, $parent));
        unset($links[$k]);
        $container = TRUE;
      }
    }
  }
  return $links;
}

